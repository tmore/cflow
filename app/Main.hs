module Main where

import Language.Flow.Ast
import Language.Flow.Graph

import Language.C.Flow.Comments
import Language.C.Flow.Extraction
import Language.C.Flow.Util
import Language.C (parseCFile)
import Language.C.System.GCC
import Language.C.Flow.Graph


import System.Environment

import Text.Show.Pretty

import qualified Options.Applicative as OPT
import           Options.Applicative hiding (Parser)

import System.FilePath ((</>), (<.>))
import Data.Maybe
import Control.Monad
import System.Exit

data Options
  = Options
      { noDot :: Bool
      -- ^ If dot should be outputted, defult should be
      -- true
      , clusterDotGraphs :: Bool
      -- ^ If the dot output should be one big
      -- graph for the program or multiple files,
      -- one for each method.
      , printFlow :: Bool
      -- ^ Print the flow representation of the program to stdout
      , saveFlow :: Bool
      -- ^ Saves the output in a file
      , insertAnnotationPoints :: Bool
      -- ^ Wether annotation points should be inserted if missing or not
      , annotationRegex :: Maybe String
      -- ^ A regex that will be run on the content of comments, used to decider
      -- wether the comment is counted as annotation or not.
      , showBuiltin :: Bool
      -- ^ If bultin functions should be ignored or not
      , queryDependencies :: Bool
      -- ^ If there should be an attempt to query all dependencies of the input
      -- file.
      , outDirPath :: FilePath
      -- ^ Path to where results should be stored.
      , language :: String
      -- ^ The languge to extract
      , cCompiler :: String
      -- ^ Name of the compiler to use
      , inputFile :: FilePath
      }
  deriving (Show, Read, Eq)

parseOptions :: OPT.Parser Options
parseOptions =
  Options
    <$> switch (mconcat [ long "no-dot"
                        , help "Output dot or not"
                        ])
    <*> switch (mconcat [ long "cluser-dot-graphs"
                        , help "If set, the output file will be one big dot graph where methods are represented by clusters"
                        ])
    <*> switch (mconcat [ long "print-flow"
                        , help "If set the flow representation will be printed to stdout"
                        ])
    <*> switch (mconcat [ long "save-flow"
                        , help "If set the flow representation will be saved to file"
                        ])
    <*> switch (mconcat [ long "insert-controlpoints"
                        , help "If set, annotation points will be inserted in the source code"
                        ])
    <*> (optional
          (strOption (mconcat [ long "annotation-regex"
                              , long "ar"
                              , help "Regex used to determine wether a comment is an annotation or not"
                              ]))
        )
    <*> switch (mconcat [ long "use-builtin"
                        , help "If builtin methods should be captured or not"
                        ])
    <*> switch (mconcat [ long "query-dependecies"
                        , help "If dependencies of the particular procedure should be included or not"
                        ])
    <*> strOption (mconcat [ long "out-dir"
                           , help "Directory where results should be stored"
                           , value "out/"
                           ])
    <*> (strOption
          (mconcat [ long "lang"
                   , help "Language of the file (Currently only C supported)"
                   , value "C"
                   ])
        )
    <*> (strOption
         (mconcat [long "c-compiler"
                  , help "The c compiler to use (if applicative)"
                  , value "gcc"
                  ]
         ))
    <*> (argument str (metavar "INPUT FILE"))

pOpts =
      info
        (parseOptions <**> helper)
        ( fullDesc <> progDesc "CFlow, methodgraph extractor"
            <> header "CFlow usage:"
        )

main :: IO ()
main = do
  options <- execParser pOpts
  eTu <- parseCFile (newGCC (cCompiler options)) Nothing [] (inputFile options)
  comments <- extractComments (inputFile options)
  case eTu of
      Left errs -> do
        putStrLn "The following errors where encountered when reading the C file:"
        putStrLn $ ppShow errs
        exitFailure
      Right tu -> do
        case extractFlowProgram tu of
          Left fErrs -> do
            putStrLn "The following errors where encountered when extracting the flow:"
            putStrLn $ ppShow fErrs
            exitFailure
          Right flowP -> do
            putStrLn $ ppShow comments
            when (not (noDot options)) $
              saveFlowProgramDotFiles (outDirPath options) flowP
            when (printFlow options) $
              putStrLn $ ppShow flowP
