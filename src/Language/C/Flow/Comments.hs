-- | Author: Tomas Möre 2019
--
-- Since the language-c library doesn't handle comments we have to do so here.
{-# LANGUAGE OverloadedStrings #-}
module Language.C.Flow.Comments where


import Language.C
import Language.C.Data.Position

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.IO as T
import Data.Void
import Data.String

import System.FilePath

import Text.Megaparsec

import Control.Monad.Extra

-- | Takes a filepath and extracts all the comments, each comment is identified
-- by its position of the character initiating the comment.
extractComments :: FilePath -> IO (Map NodeInfo Text)
extractComments fp = do
  contents <- T.readFile fp
  let assocComments = parse (many (try commentParser)) fp contents
  pure $ Map.fromList $ either (error "Should not happend") id assocComments
  where
    commentParser :: Parsec Void Text (NodeInfo, Text)
    commentParser = do
      match <- skipManyTill anySingle (("/" >> try ("/" <|> "*")))
      let (endParser, endLength) =
              case match of
                "/" -> ("\n", 1)
                "*" ->  ("*/", 2)
                _ -> error "Should not happend"

      pos <- getSourcePos
      comment <- manyTill anySingle (try endParser)
      let nodePos = position 0 fp (unPos $ sourceLine pos) (unPos $ sourceColumn pos) Nothing
      pure (OnlyPos nodePos (nodePos, length comment + endLength)
           , fromString comment
           )
