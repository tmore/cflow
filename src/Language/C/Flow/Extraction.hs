-- | Author: Tomas Möre 2019
-- Gives a set of procedure to extract the flow program from a C program
{-# LANGUAGE FlexibleContexts #-}
module Language.C.Flow.Extraction where


import Language.C.Analysis
import Language.C

import Control.Monad.Writer

import Language.Flow.Ast
import Language.Flow.Writer

import Language.C.Flow.Util

import Data.Functor.Identity

data TranslError =
  CErrors [CError]
  | TranslError [String]
  deriving (Show)

-- | Translates a translation unit to a flow program
extractFlowProgram :: CTranslUnit -> Either TranslError (FlowProgram NodeInfo Ident)
extractFlowProgram tu = do
  globalDecls <- either (Left . CErrors) Right  $ globalObjects tu
  let methods = getFunctonStms globalDecls
  pure $ fmap extractProcedure methods

-- | Extract the start and end annotations of a c program given the body
-- statement (That must be a compund expression)
extractProcedure :: Stmt -> FlowProcedure NodeInfo Ident
extractProcedure stmt@(CCompound _ blockItems annot) =
  let (Identity body) = execWriterT (extractCStatement stmt)
  in FlowProcedure annot body (Language.C.annotation (last blockItems))
extractProcedure _ =
  error "Can't extract a procedure from non compund statements"

-- | Extracts the flow of a C statement.
extractCStatement :: FlowWriter NodeInfo Ident m => Stmt -> m ()
extractCStatement (CLabel _ stm _ _) =
  extractCStatement stm
extractCStatement (CCase _ stm _) =
  extractCStatement stm
extractCStatement (CCases _ _ stm _) =
  extractCStatement stm
extractCStatement (CDefault stm _) =
  extractCStatement stm
extractCStatement (CExpr (Just expr) _) =
  extractCExpr expr
extractCStatement (CExpr Nothing _) =
  pure ()
extractCStatement (CCompound _ blockItems _) = do
  forM_ blockItems $ \ bi ->
    case bi of
      CBlockStmt stmt ->
        extractCStatement stmt
      CBlockDecl decl ->
        extractCDecl decl
      CNestedFunDef _ ->
        error "Nested function defenitions not added yet"

extractCStatement (CIf guard trueCase mFalseCase info) = do
  extractCExpr guard
  extractBranch info
                (extractCStatement trueCase)
                (mapM_ extractCStatement mFalseCase)
extractCStatement (CSwitch choiceExpr stmt info) =
  error "Not implemented yet because of trickeries"
extractCStatement (CWhile guard bodyStmt isDoWhile info) = do
  when isDoWhile (error "Do while not supported yet")
  extractLoop info
              (extractCExpr guard)
              (extractCStatement bodyStmt)
extractCStatement (CFor initOrDecl mGuard mIncrement bodyStmt info) = do
  either (mapM_ extractCExpr) extractCDecl initOrDecl
  extractLoop info
              (mapM_ extractCExpr mGuard)
              (extractCStatement bodyStmt >> incrementStmt)
  where
      incrementStmt = mapM_ extractCExpr mIncrement
extractCStatement (CGoto _ _) =
  error "Goto's not yet supported"
extractCStatement (CGotoPtr _ _) =
  error "Goto's not yet supported"
extractCStatement (CCont info) =
  tell [Continue info]
extractCStatement (CBreak info) =
  tell [Break info]
extractCStatement (CReturn mExpr info) = do
  mapM_ extractCExpr mExpr
  tell [Return info]
extractCStatement (CAsm _ _) =
  error "ASM not yet supported"

extractCExpr :: FlowWriter NodeInfo Ident m => Expr -> m ()
extractCExpr (CComma exprs _) =
  void $ traverse extractCExpr exprs
extractCExpr (CAssign _ referenceExpr valueExpr _) = do
  extractCExpr referenceExpr
  extractCExpr valueExpr
extractCExpr (CCond guard mTrueCase falseCase info) = do
  extractCExpr guard
  extractBranch info
                (void $ traverse extractCExpr mTrueCase)
                (extractCExpr falseCase)

-- | When executing and and operation the right expression will only be executed if
-- the left part is true
extractCExpr (CBinary CLndOp leftExpr rightExpr info) = do
  extractCExpr leftExpr
  extractBranch info (extractCExpr rightExpr) (pure ())

extractCExpr (CBinary CLorOp leftExpr rightExpr info) = do
  (extractCExpr leftExpr)
  extractBranch info (pure ()) (extractCExpr rightExpr)
extractCExpr (CBinary _ leftExpr rightExpr _) = do
  extractCExpr leftExpr
  extractCExpr rightExpr
extractCExpr (CCast decl expr _) = do
  extractCDecl decl
  extractCExpr expr
extractCExpr (CUnary _ expr _) =
  extractCExpr expr
extractCExpr (CSizeofExpr _ _) =
  pure ()
extractCExpr (CSizeofType _ _) =
  pure ()
extractCExpr (CAlignofExpr _ _) =
  pure ()
extractCExpr (CAlignofType _ _) =
  pure ()
extractCExpr (CComplexReal expr _) =
  extractCExpr expr
extractCExpr (CComplexImag expr _) =
  extractCExpr expr
extractCExpr  (CIndex ptrExpr indexExpr _) = do
  extractCExpr ptrExpr
  extractCExpr indexExpr
extractCExpr (CCall (CVar ident _) params info) = do
  mapM_ extractCExpr params
  tell [Call info (Known ident)]
-- | Note to self if we're calling an annonimous expression we may not be able
-- to analyze it? Will replace non deterministic function calls with Nothing)
-- This should probabily be changed such that it tries to retrieve all known
-- function calls.
extractCExpr (CCall expr params info) = do
  extractCExpr expr
  mapM_ extractCExpr params
  tell [Call info Unknown]
extractCExpr (CMember structSelector _ _ _) =
  extractCExpr structSelector
extractCExpr (CVar _ _) =
  pure ()
extractCExpr (CConst _) =
  pure ()
extractCExpr (CCompoundLit decl initList _) = do
  extractCDecl decl
  extractCInitializerList initList
extractCExpr (CGenericSelection _ _ _) =
  error "Generic selection should be compile time instruction"
extractCExpr (CStatExpr stm _) =
  extractCStatement stm
extractCExpr (CLabAddrExpr ident info) =
  tell [Label info ident]
extractCExpr (CBuiltinExpr _) =
  error "Builtin expressions not handled yet"

extractCDecl :: FlowWriter NodeInfo Ident m => CDeclaration NodeInfo -> m ()
extractCDecl (CDecl _ decls _) = do
  forM_ decls $ \ (_,mInitializer, mExpression) -> do
    mapM_ extractCInitializer mInitializer
    mapM_ extractCExpr mExpression
extractCDecl (CStaticAssert expr _ _) =
  extractCExpr expr

extractCInitializer :: FlowWriter NodeInfo Ident m => CInitializer NodeInfo -> m ()
extractCInitializer (CInitExpr expr _) =
  extractCExpr expr
extractCInitializer (CInitList initList _) =
  extractCInitializerList initList


extractCInitializerList :: FlowWriter NodeInfo Ident m => CInitializerList NodeInfo -> m ()
extractCInitializerList inits = do
  forM_ inits $ \(_, initializer) -> do
    -- Designators must be constant expression and are ingored
    extractCInitializer initializer
