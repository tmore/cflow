{-# LANGUAGE OverloadedStrings #-}

module Language.C.Flow.Graph where

import Language.Flow.Graph
import Language.Flow.Ast hiding (Flow(..))
import Language.C.Flow.Extraction

import Language.C (NodeInfo, Ident, identToString, posOfNode, posRow, posColumn)

import Data.GraphViz

import Data.Graph.Inductive.Graph
import Data.GraphViz.Printing (renderDot)

import Data.Text.Lazy (Text)
import Data.Text.Lazy as T
import Data.Text.Lazy.IO as T
import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad

import System.FilePath
import System.Directory
import Data.String

import Data.GraphViz.Attributes.Complete

-- | The default writing method for showing the boundaries of the blocks and
-- the called methods of the graphs.
printDotParams :: GraphvizParams Node (NodeInfo,NodeInfo) (Maybe (CallInfo NodeInfo Ident)) () (NodeInfo,NodeInfo)
printDotParams =
  nonClusteredParams { fmtNode = nodeToAttrs
                     , fmtEdge = edgeToAttrs

                     }
  where
    edgeToAttrs (_,_,Nothing) = []
    edgeToAttrs (_,_,Just (CallInfo ni (Known ident))) =
                    [Label (StrLabel $ fromString (identToString ident))]
    edgeToAttrs (_,_,Just (CallInfo ni Unknown)) =
                    [Label (StrLabel $ "?\n" <> displayPos ni)]
    nodeToAttrs (0, (preNi, _)) =
                    [Label (StrLabel $ "Start\n" <> displayRow preNi )]
    nodeToAttrs (1, (preNi, _)) =
                    [Label (StrLabel $ "Return\n" <> displayRow preNi )]
    nodeToAttrs (nodeId, (preNi, postNi)) =
                    [Label (StrLabel $ mconcat ["Pre: "
                                               , displayPos preNi
                                               , "\nPost: "
                                               , displayPos postNi
                                               ])]
    displayRow ni = let pos = posOfNode ni
                    in fromString $ show (posRow pos)
    displayPos ni = let pos = posOfNode ni
                    in fromString $ show (posRow pos) <> ":" <> show (posColumn pos)
dotParamsAddGraphName :: Text -> GraphvizParams n nl el cl l -> GraphvizParams n nl el cl l
dotParamsAddGraphName name gp =
   gp{ globalAttributes =
         globalAttributes nonClusteredParams <>  [GraphAttrs $ [Label (StrLabel name)]]
     }

-- | Converts a flow program to a map of DotGraph nodes.
--
-- Note: Doesn't currently handle lables in a good way
flowProgramToDotGraph :: FlowProgram NodeInfo Ident -> Map Ident (DotGraph Node)
flowProgramToDotGraph fp =
  Map.mapWithKey go (flowProgramToPTGraphs fp)
  where
    go k procedure =
      let dp = dotParamsAddGraphName (fromString (identToString k))
                                     printDotParams
      in graphToDot dp procedure
-- | Converts a flow program to a map of identifiers and the dot text
-- representing the graphs
flowProgramToDotText :: FlowProgram NodeInfo Ident -> Map Ident Text
flowProgramToDotText = fmap (renderDot . toDot) . flowProgramToDotGraph


-- | Takes a flow program and stores the output filepaths to disk in a given
-- directory. Each dot graph will have the name of each respective function.
saveFlowProgramDotFiles :: FilePath -> FlowProgram NodeInfo Ident -> IO ()
saveFlowProgramDotFiles dirFP flowProgram = do
  createDirectoryIfMissing True dirFP
  forM_ (Map.assocs dotTexts) $ \ (funcIdent, dotText) -> do
    let fp = (dirFP </> identToString funcIdent <.> ".dot")
    T.writeFile fp dotText
  where
    dotTexts = flowProgramToDotText flowProgram
