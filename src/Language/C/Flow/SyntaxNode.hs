-- | This module defines a datatype meant to capture all C terms as one

-- datatype, enabling a somewhat more generic way to explore the syntaxtree.
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Language.C.Flow.SyntaxNode where


import Language.C
import Language.C.Data.Ident

import Data.Tree

-- | Only captures the syntax elements that has a unique annotation
data SyntaxNode a =
    SNStringLiteral (CStringLiteral a)
  | SNConstant (CConstant a)
  | SNBuiltinThing (CBuiltinThing a)
  | SNExpression (CExpression a)
  | SNAttribute (CAttribute a)
  | SNPartDesignator (CPartDesignator a)
  | SNInitializer (CInitializer a)
  | SNEnumeration (CEnumeration a)
  | SNStructureUnion (CStructureUnion a)
  | SNAlignmentSpecifier (CAlignmentSpecifier a)
  | SNFunctionSpecifier (CFunctionSpecifier a)
  | SNTypeQualifier (CTypeQualifier a)
  | SNTypeSpecifier (CTypeSpecifier a)
  | SNStorageSpecifier (CStorageSpecifier a)
  | SNAssemblyOperand (CAssemblyOperand a)
  | SNAssemblyStatement (CAssemblyStatement a)
  | SNStatement (CStatement a)
  | SNDerivedDeclarator (CDerivedDeclarator a)
  | SNDeclarator (CDeclarator a)
  | SNDeclaration (CDeclaration a)
  | SNFunctionDef (CFunctionDef a)
  | SNTranslationUnit (CTranslationUnit a)
  | SNIdent Ident a
  | SNAsmExt (CStringLiteral a) a
  -- ^ Addhoc parameterization on a


class HasSNTree t a | t -> a where
  toSNTree :: t -> Tree (SyntaxNode a)


instance HasSNTree (CStringLiteral NodeInfo) NodeInfo where
  toSNTree t@(CStrLit _ a) = Node (SNStringLiteral t) []
instance HasSNTree (CConstant NodeInfo) NodeInfo where
  toSNTree t = Node (SNConstant t) []
instance HasSNTree (CBuiltinThing NodeInfo) NodeInfo where
  toSNTree t =
    Node (SNBuiltinThing t)
     (case t of
      CBuiltinVaArg expr decl _ -> [toSNTree expr, toSNTree decl]
      CBuiltinOffsetOf decl desig _ -> (toSNTree decl : map toSNTree desig)
      CBuiltinTypesCompatible decla declb _ -> [toSNTree decla, toSNTree declb]
      CBuiltinConvertVector expr decl _ -> [toSNTree expr, toSNTree decl]
     )
instance HasSNTree (CExpression NodeInfo) NodeInfo where
  toSNTree t = Node (SNExpression t)
    (case t of
       CComma exprs _ -> map toSNTree exprs
       CAssign _ expl expr _ -> [toSNTree expl, toSNTree expr]
       CCond guard mFalseCase trueCase _ ->
         (toSNTree guard : toSNTree trueCase : maybe [] (pure . toSNTree) mFalseCase)
       CBinary _ exprl exprr _ ->
          [toSNTree exprl, toSNTree exprr]
       CCast decl expr _ ->
         [toSNTree decl, toSNTree expr]
       CUnary _ expr _ ->
         [toSNTree expr]
       CSizeofExpr expr _ ->
         [toSNTree expr]
       CSizeofType decl _ ->
         [toSNTree decl]
       CAlignofExpr expr _ ->
         [toSNTree expr]
       CAlignofType decl _ ->
         [toSNTree decl]
       CComplexReal expr _ ->
         [toSNTree expr]
       CComplexImag expr _ ->
         [toSNTree expr]
       CIndex exprl exprr _ ->
         [toSNTree exprl, toSNTree exprr]
       CCall expr exprs _ ->
         toSNTree expr : map toSNTree exprs
       CMember expr _ _ _ ->
         [toSNTree expr]
       CVar ident _ ->
         [toSNTree ident]
       CConst const ->
         [toSNTree const]
       CCompoundLit decl initList _ ->
         ([toSNTree decl] ++ initListToForest initList)
       CGenericSelection expr items  _ ->
         toSNTree expr : concatMap (\ (f,s) -> maybe [] (pure . toSNTree) f ++ [toSNTree s]) items
       CStatExpr stmt _ ->
         [toSNTree stmt]
       CLabAddrExpr ident _ ->
         [toSNTree ident]
       CBuiltinExpr builtin ->
         [toSNTree builtin]
       )
instance HasSNTree (CAttribute NodeInfo) NodeInfo where
  toSNTree t@(CAttr ident exprs _) =
    Node (SNAttribute t) (toSNTree ident : (fmap toSNTree exprs))
instance HasSNTree (CPartDesignator NodeInfo) NodeInfo where
  toSNTree t = Node (SNPartDesignator t)
    (case t of
       CArrDesig expr _ -> [toSNTree expr]
       CMemberDesig ident _ -> [toSNTree ident]
       CRangeDesig exprl exprr _ -> [toSNTree exprl, toSNTree exprr]
    )
instance HasSNTree (CInitializer NodeInfo) NodeInfo where
  toSNTree t = Node (SNInitializer t)
    (case t of
       CInitExpr expr _ -> [toSNTree expr]
       CInitList initList _ -> initListToForest initList
       )
instance HasSNTree (CEnumeration NodeInfo) NodeInfo where
  toSNTree t@(CEnum mIdent mxs attrs _) =
    Node (SNEnumeration t) $
    mconcat [ mToTree mIdent
            , (maybe []
               (concatMap (\ (_, mExpr) ->
                             mToTree mExpr)
               )
               mxs)
            , map toSNTree attrs
            ]
instance HasSNTree (CStructureUnion NodeInfo) NodeInfo where
  toSNTree t@(CStruct _ mIdent mDecls attrs _) =
    Node (SNStructureUnion t) $
    concat [ mToTree mIdent
           , (maybe [] (map toSNTree) mDecls)
           , map toSNTree attrs
           ]
instance HasSNTree (CAlignmentSpecifier NodeInfo) NodeInfo where
  toSNTree t = Node (SNAlignmentSpecifier t) $
    (case t of
       CAlignAsType decl _ -> [toSNTree decl]
       CAlignAsExpr expr _ -> [toSNTree expr]
    )
instance HasSNTree (CFunctionSpecifier NodeInfo) NodeInfo where
  toSNTree t = Node (SNFunctionSpecifier t) []
instance HasSNTree (CTypeQualifier NodeInfo) NodeInfo where
  toSNTree t = Node (SNTypeQualifier t) $
    (case t of
       CAttrQual attrib -> [toSNTree attrib]
       _ -> []
    )
instance HasSNTree (CTypeSpecifier NodeInfo) NodeInfo where
  toSNTree t = Node (SNTypeSpecifier t) $
    (case t of
       CSUType su _ -> [toSNTree su]
       CEnumType enum _ -> [toSNTree enum]
       CTypeDef ident _ -> [toSNTree ident]
       CTypeOfExpr expr _ -> [toSNTree expr]
       CTypeOfType decl _ -> [toSNTree decl]
       CAtomicType decl _ -> [toSNTree decl]
       _ -> []
    )
instance HasSNTree (CStorageSpecifier NodeInfo) NodeInfo where
  toSNTree t = Node (SNStorageSpecifier t) []
instance HasSNTree (CAssemblyOperand NodeInfo) NodeInfo where
  toSNTree t@(CAsmOperand mIdent slit expr _) =
    Node (SNAssemblyOperand t) $ (mToTree mIdent) ++ [toSNTree slit, toSNTree expr]
instance HasSNTree (CAssemblyStatement NodeInfo) NodeInfo where
  toSNTree t@(CAsmStmt mTQ slit lops rops slits _)
    = Node (SNAssemblyStatement t) $
      concat [ maybe [] (pure . toSNTree) mTQ
             , [toSNTree slit]
             , map toSNTree lops
             , map toSNTree rops
             , map toSNTree slits
             ]
instance HasSNTree (CStatement NodeInfo) NodeInfo where
  toSNTree t = Node (SNStatement t) $
    (case t of
       CLabel ident stmt attrs _ ->
         toSNTree ident : toSNTree stmt : map toSNTree attrs
       CCase expr stmt _ ->
         [toSNTree expr, toSNTree stmt]
       CCases expr1 expr2 stmt _ ->
         [toSNTree expr1, toSNTree expr2, toSNTree stmt]
       CDefault stmt _ ->
         [toSNTree stmt]
       CExpr mExpr _ ->
         maybe [] (pure . toSNTree) mExpr
       CCompound _ blockItems _ ->
         map blockItemToTree blockItems
       CIf guardExpr trueStmt falseStmt _ ->
         (toSNTree guardExpr: toSNTree trueStmt: mToTree falseStmt)
       CSwitch expr stmt _ ->
         [toSNTree expr, toSNTree stmt]
       CWhile expr stmt _ _ ->
         [toSNTree expr, toSNTree stmt]
       CFor eMExprDecl mExpr1 mExpr2 stmt _ ->
         concat [either mToTree (pure . toSNTree) eMExprDecl
                , mToTree mExpr1
                , mToTree mExpr2
                , [toSNTree stmt]
                ]
       CGoto ident _ ->
         [toSNTree ident]
       CGotoPtr expr _ ->
         [toSNTree expr]
       CCont _ -> []
       CBreak _ -> []
       CReturn mExpr _ ->
         mToTree mExpr
       CAsm asmStmt _ ->
         [toSNTree asmStmt]
       )
instance HasSNTree (CDerivedDeclarator NodeInfo) NodeInfo where
  toSNTree t = Node (SNDerivedDeclarator t)
    (case t of
       CPtrDeclr tquals _ -> map toSNTree tquals
       CArrDeclr tquals arrSize _ ->
         map toSNTree tquals
         ++ (case arrSize of
               CNoArrSize _ -> []
               CArrSize _ expr -> [toSNTree expr]
            )
       CFunDeclr eIdentsDecls attrs _ ->
         either (map toSNTree) (map toSNTree . fst) eIdentsDecls
         ++ map toSNTree attrs
    )
instance HasSNTree (CDeclarator NodeInfo) NodeInfo where
  toSNTree t@(CDeclr mIdent derivedDecls mStr attrs _) =
    Node (SNDeclarator t) $
     (concat [ mToTree mIdent
             , map toSNTree derivedDecls
             , mToTree mStr
             , map toSNTree attrs
             ]
     )
instance HasSNTree (CDeclaration NodeInfo) NodeInfo where
  toSNTree t = Node (SNDeclaration t) $
    (case t of
       CDecl declSpecifiers stuff _ ->
         concat [ map declarationSpecifierToTree declSpecifiers
                , concat $ flip map stuff $ \ (mDecl, mInit, mExpr) ->
                    concat [ mToTree  mDecl
                           , mToTree mInit
                           , mToTree mExpr
                           ]
                ]
       CStaticAssert expr str _ ->
         [toSNTree expr, toSNTree str]
    )
instance HasSNTree (CFunctionDef NodeInfo) NodeInfo where
  toSNTree t@(CFunDef declSpecs decl decls stmt _) =
    Node (SNFunctionDef t) $ concat [ map declarationSpecifierToTree declSpecs
                                    , [toSNTree decl]
                                    , map toSNTree decls
                                    , [toSNTree stmt]
                                    ]
instance HasSNTree (CTranslationUnit NodeInfo) NodeInfo where
  toSNTree t@(CTranslUnit extDecls _) =
    Node (SNTranslationUnit t) $
    flip concatMap extDecls (\ e ->
                            case e of
                              CDeclExt decl -> [toSNTree decl]
                              CFDefExt fdef -> [toSNTree fdef]
                              CAsmExt strLit a ->
                                [Node (SNAsmExt strLit a) [toSNTree strLit]]
                            )
instance HasSNTree Ident NodeInfo where
  toSNTree t@(Ident _ _ ni) = Node (SNIdent t ni) []

initListToForest :: CInitializerList NodeInfo -> Forest (SyntaxNode NodeInfo)
initListToForest =
  concatMap (\ (designators, initailier) ->
               map toSNTree designators ++ [toSNTree initailier]
            )


declarationSpecifierToTree :: (CDeclarationSpecifier NodeInfo) -> Tree (SyntaxNode NodeInfo)
declarationSpecifierToTree t =
  case t of
    CStorageSpec x -> toSNTree x
    CTypeSpec x -> toSNTree x
    CTypeQual x -> toSNTree x
    CFunSpec x -> toSNTree x
    CAlignSpec x -> toSNTree x


blockItemToTree :: CCompoundBlockItem NodeInfo -> Tree (SyntaxNode NodeInfo)
blockItemToTree t =
  case t of
    CBlockStmt x -> toSNTree x
    CBlockDecl x -> toSNTree x
    CNestedFunDef def -> toSNTree def


mToTree :: HasSNTree x a => Maybe x -> Forest (SyntaxNode a)
mToTree Nothing = []
mToTree (Just x) = [toSNTree x]
