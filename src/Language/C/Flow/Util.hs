module Language.C.Flow.Util where

import Language.C.Analysis
import Language.C
import Language.C.Parser
import Language.C.System.GCC

import Data.Map (Map)
import qualified Data.Map as Map

functionStmsFromFile :: String -> IO (Map Ident Stmt)
functionStmsFromFile fp = do
  tu <- either (error "shit") id  <$> parseCFile (newGCC "gcc") Nothing [] fp
  let eGO = globalObjects tu
  case eGO of
    Left errs -> error $ show errs
    Right go -> pure $ getFunctonStms go

globalObjects :: CTranslUnit -> Either [CError] GlobalDecls
globalObjects = fmap (fst) . runTrav_ . analyseAST

getFunctonStms :: GlobalDecls -> Map Ident Stmt
getFunctonStms gd =
  fmap (\ (FunctionDef (FunDef _ stmt _)) -> stmt) functionDefs
  where
    origMap = gObjs gd
    functionDefs = Map.filter funDefFilter origMap

    funDefFilter (FunctionDef _) = True
    funDefFilter _ = False
