-- | Author: Tomas Möre 2019
{-# LANGUAGE StandaloneDeriving #-}

module Language.Flow.Ast where

import Data.Map (Map)

data ProcInfo a = Unknown
                | Known a
                deriving (Show, Read, Eq)


-- | The ast of the flow language The paremeter is pretty much just some
-- annotation. However it may be used to for example relate the flow language
-- elements to some part of the original source code.
data Flow a i = Call a (ProcInfo i)
              -- ^ Calls to methods
              | Assign a i
              -- ^ Assignemnt to variables, should only capture variables that
              -- change the global state.
              | Break a
              | Continue a
              | Return a
              | Label a i
              | Goto a i
              | Branch a [Flow a i] [Flow a i]
              -- ^ [Pre if annotation], [post if annotation], [True case], [False case]
              | Loop a [Flow a i] [Flow a i]
              -- ^ [Pre while annotation (invariant)], [Post while annotation], [guard], [body]
              -- Left is statement responsible for
              -- the branch condition right is the
              -- body

deriving instance (Show a, Show i) => Show (Flow a i)
deriving instance (Read a, Read i) => Read (Flow a i)
deriving instance (Eq a, Eq i) => Eq (Flow i a)

-- | A flow procedure consists of a start node, and end node and a body. The
-- annotation of the startnode and end node is implementation specific.
data FlowProcedure a i =
  FlowProcedure { _startNode :: a
                , _body :: [Flow a i]
                , _endNode :: a
                }
deriving instance (Show a, Show i) => Show (FlowProcedure a i)
deriving instance (Read a, Read i) => Read (FlowProcedure a i)
deriving instance (Eq a, Eq i) => Eq (FlowProcedure a i)


type FlowProgram a i =
  Map i (FlowProcedure a i)

annotation :: Flow a i ->  a
annotation (Call a _) = a
annotation (Assign a _) = a;
annotation (Break a) = a
annotation (Continue a) = a
annotation (Return a) = a
annotation (Label a _) = a
annotation (Goto a _) = a
annotation (Branch a _ _) = a
annotation (Loop a _ _) = a
