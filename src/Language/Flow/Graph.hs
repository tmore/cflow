-- | Author: Tomas Möre 2019
--
-- This module is responsible for translating the Flow language into a graph
-- That may be used in order to export the result elsewhere
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections #-}
module Language.Flow.Graph
  ( flowProgramToPTGraphs
  , flowProgramToGraphs
  , flowProcedureToGraph
  , CallInfo(..)
  )where


import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.PatriciaTree
import Data.Maybe

import Language.Flow.Ast

import Control.Monad.State

import Data.Functor.Identity
import Data.Map (Map)

flowProgramToPTGraphs :: FlowProgram a i -> Map i (Gr (a,a) (Maybe (CallInfo a i)))
flowProgramToPTGraphs = flowProgramToGraphs

flowProgramToGraphs :: ( DynGraph gr
                       , Graph gr)
                    => FlowProgram a i -> Map i (gr (a,a) (Maybe (CallInfo a i)))
flowProgramToGraphs =
  fmap flowProcedureToGraph

-- | Converts a FlowProcedure to a graph, allways inserts a start end end node
-- of the graph that will be tagged with the annotation given in the
-- FlowProcedure argument.
flowProcedureToGraph :: ( DynGraph gr
                        , Graph gr)
                     => FlowProcedure a i
                     -> gr (a,a) (Maybe (CallInfo a i))
flowProcedureToGraph procedure =
  let (Identity gr) = do
        execStateT writeFlowProgram defaultGraphContext
  in _graph gr
  where
    writeFlowProgram = do
      startNode <- newNode (_startNode procedure, _startNode procedure)
      endNode <- newNode (_endNode procedure, _endNode procedure)
      modify $ \ s -> s{ _afterNode = (endNode, Nothing)
                       , _returnNode = endNode
                       }
      (bodyNode, bodyLbl) <- writeFlowGraph $ _body procedure
      tellEdge (startNode, bodyNode, bodyLbl)
      pure ()


-- | The context is needed in order to direct the graph to the right destination
-- when constructing graphs from sub sequences of instructions
data GraphContext g a i =
  GraphContext
  { _afterNode :: (Node, Maybe (CallInfo a i))
  , _continueNode :: Maybe (Node, Maybe (CallInfo a i))
  , _breakNode :: Maybe (Node, Maybe (CallInfo a i))
  , _returnNode :: Node
  , _nextNodeId :: Node
  , _graph :: g
  }



defaultGraphContext :: Graph gr => GraphContext (gr (a,a) (Maybe (CallInfo a i))) a i
defaultGraphContext = GraphContext (0,Nothing) Nothing Nothing 0 0 empty

data CallInfo a i =
  CallInfo a (ProcInfo i)
  deriving (Show, Read, Eq)

type GraphGenM a i gr m = ( DynGraph gr
                        , Graph gr
                        , MonadState (GraphContext (gr (a,a) (Maybe (CallInfo a i))) a i) m
                        )
newNode :: GraphGenM a i gr m => (a, a) -> m Node
newNode lbl = do
  node <- gets _nextNodeId
  modify (\ s -> s{_nextNodeId = node + 1
                  , _graph = insNode (node, lbl) (_graph s)
                  })
  pure node

tellEdge :: GraphGenM a i gr m
         => LEdge (Maybe (CallInfo a i)) -> m ()
tellEdge edge = do
  modify (\ s -> s{ _graph = insEdge edge (_graph s)
                  })
  pure ()

getNodeLabel :: GraphGenM a i gr m => Node -> m (a,a)
getNodeLabel n = do
  g <- gets _graph
  pure $ fromJust $ lab g n

updateNode :: GraphGenM a i gr m => Node -> (a,a) -> m ()
updateNode n lbl = do
  modify $ \ st ->
    st{ _graph =
        case match n (_graph st) of
          (Just (p, _, _, s), cg) -> (p, n, lbl, s) & cg
          (Nothing, _) -> error "Node does not exist"
     }
-- | Constructs a node from the first two elements in the list, or if the list
-- only has one element, from the element and the after node annotation.
-- mkNode :: GraphGenM a gr m => NonEmpty (Flow a) -> m Node
-- mkNode (t :| []) = do
--   (_,)gets _afterNode

-- | Allways returns an edge to which the previous node should connect to.
writeFlowGraph :: GraphGenM a i gr m => [Flow a i] -> m (Node, Maybe (CallInfo a i))
-- | This case handles when there's two subsequent calls. Here we create an auxialary node in between and mergin the edges
writeFlowGraph [] = do
  gets _afterNode
writeFlowGraph (Call annot procInfo : xs) = do
  (nextNode, nextLabel) <- writeFlowGraph xs
--  (_,nextNodePostLbl) <- getNodeLabel nextNode
--  updateNode nextNode (annot, nextNodePostLbl)
  case nextLabel of
    Nothing -> pure (nextNode, Just (CallInfo annot procInfo))
    e@(Just (CallInfo nextAnnot _)) -> do
      node <- newNode (annot, nextAnnot)
      tellEdge (node, nextNode, nextLabel)
      pure (node, Just (CallInfo annot procInfo))
writeFlowGraph (Break ni : _) = do
  mBreakNode <- gets _breakNode
  case mBreakNode of
    Nothing ->
      error "Break appeared in a non valid context"
    Just breakNode ->
      pure breakNode
writeFlowGraph (Continue a : xs) = do
  mContinueNode <- gets _continueNode
  case mContinueNode of
    Nothing ->
      error "Contue appeared in a non valid context"
    Just continueNode -> pure continueNode

writeFlowGraph (Return a : xs) = do
  (,Nothing) <$> gets _returnNode
writeFlowGraph (Label a _ : xs) = do
  error "Labels not supported yet"
writeFlowGraph (Goto a _ : xs) = do
  error "Goto not supported yet"
writeFlowGraph (Branch a trueCase falseCase : xs) = do
  localAfter <- writeFlowGraph xs
  modify $ \ s -> s {_afterNode = localAfter}
  (tcFirst, tcLbl) <- writeFlowGraph trueCase
  (fcFirst, fcLbl) <- writeFlowGraph falseCase
  node <- newNode (a, a)
  tellEdge (node, tcFirst, tcLbl)
  tellEdge (node, fcFirst, fcLbl)
  pure (node, Nothing)
writeFlowGraph (Loop a guard body : xs) = do
  origState <- get
  next@(nextNode, nextLbl) <- writeFlowGraph xs
  branchNode <- newNode (a,a)
  modify $ \ s -> s{_afterNode = (branchNode, Nothing)}
  (guardStart, guardLbl) <- writeFlowGraph guard
  preGuardNode <-
    case guardLbl of
      Nothing -> pure guardStart
      Just _ -> do
        pgNode <- newNode (a,a)
        tellEdge (pgNode, guardStart, guardLbl)
        pure pgNode

  modify $ \ s -> s{ _afterNode = (preGuardNode, Nothing)
                   , _continueNode = Just (preGuardNode, Nothing)
                   , _breakNode = Just next
                   }
  (loopNode, loopLbl) <- writeFlowGraph body
  tellEdge (branchNode, nextNode, nextLbl)
  tellEdge (branchNode, loopNode, loopLbl)
  s <- get
  put origState{ _nextNodeId = _nextNodeId s
               , _graph = _graph s
               }
  pure (preGuardNode, Nothing)








-- makeNodes :: GraphGenM a gr m => [Flow a] -> m Node
-- makeNodes flow = do
--   nodes <- forM nodePairs $ \ p@(first, second) -> do
--     newNode $ biMap annotation p

--   where
--     nodePairs = zip flow (tail flow)
