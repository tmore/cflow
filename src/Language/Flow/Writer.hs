{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Language.Flow.Writer where

import Control.Monad.Writer

import Language.Flow.Ast

import Data.Sequence (Seq)

type FlowWriter a i m = SubMonadWriter [Flow a i] m

-- | The name is shitty, but we need to say that we may execute a writing
-- procedure without writing to the main output
class (Monoid w, MonadWriter w m) => SubMonadWriter w m | m -> w where
  internalWriter :: m a -> m (a, w)

instance (Eq w, Monad m, Monoid w) => SubMonadWriter w (WriterT w m) where
  internalWriter ma =
    lift $ runWriterT ma

-- | Checks wether a branching position should be added or not. It should only
-- be inserted if the inner part of the possible branches actually have some
-- interesting events
extractBranch :: FlowWriter a i m => a -> m () -> m () -> m ()
extractBranch info trueCase falseCase = do
  leftCase <- snd <$> internalWriter trueCase
  rightCase <- snd <$> internalWriter falseCase
  if null leftCase && null rightCase
    then pure ()
    else tell $ pure (Branch info leftCase rightCase)

-- | Checks wether
extractLoop :: FlowWriter a i m => a -> m () -> m () -> m ()
extractLoop info guardExpr bodyExpr = do
  guard <- snd <$> internalWriter guardExpr
  body <- snd <$> internalWriter bodyExpr
  if null guard && null body
    then pure ()
    else tell $ pure (Loop info guard body)
